import React, { Component } from 'react';
import firebase from 'firebase';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Text } from 'native-base';
import {
    KeyboardAvoidingView,
    StyleSheet,
    TextInput,
    View, TouchableOpacity, FlatList
  } from 'react-native';


export default class Detail extends Component {

  
    constructor(props){
        super(props)

        this.state = {
            Title: this.props.navigation.state.params.Id,
            comment: '',
            name: '',
            userList: [],
            commentList: []
        };

    }
    
    componentWillMount(){
        var config = {
            apiKey: "AIzaSyAX_l-VCWpPjlhrMsJANmJcxD_8vXA1x8E",
            authDomain: "movieproject-272b8.firebaseapp.com",
            databaseURL: "https://movieproject-272b8.firebaseio.com",
            projectId: "movieproject-272b8",
            storageBucket: "movieproject-272b8.appspot.com",
            messagingSenderId: "399022506889"
        };
       
        if (!firebase.apps.length) {
            firebase.initializeApp(config);
        }

        this.displayComments();
    }

    onChangeName = textC => this.setState({ name: textC });
    onChangeComment = textC => this.setState({ comment: textC });

    submit = () => {
        if (this.state.name && this.state.comment) {
           this.insertComments();

        } else {
          alert('Please enter your comment or username first');
        }
      };

    insertComments = () => {
        firebase.database().ref('movies/'+this.state.Title+"/"+this.state.name).set({
                name: this.state.name,
                comment: this.state.comment
            }
        ).then(() => {
            alert('Successful!')
            this.displayComments();
        }).catch((error) => {
            console.log(error)
            
        });    
    }

    displayComments = () => {
        var counter = 0;
        var comments = [];
        var username = [];

       firebase.database().ref('movies/'+this.state.Title).on('value', (data) => {
            if(!data.toJSON()){
                alert('be the first to comment!')
            }

            else {
                var dataLength = Object.keys(data.toJSON()).length
                data.forEach(element => {    
                    username.push(element.toJSON().name)
                    comments.push(element.toJSON().comment)   
                
                    counter++;

                    if(counter === dataLength){
                        this.setState({
                        userList: username,
                        commentList: comments
                        })
                    }
                })
            }
           
            })
        }
    
    render(){
        const keyboardVerticalOffset = Platform.OS === 'ios' ? 64 : 0

        var user = this.state.userList;
        var comment = this.state.commentList;

        const displayList = user.map((name, i) =>  {
            return (
            <ListItem avatar>
                <Body>
                    <Text> {user[i]} </Text>
                    <Text note> {comment[i]} </Text> 
                </Body>
            </ListItem>
            )
        })


        return (   
            <Container>
                <Header />
                <Content>
                <List>
                    {displayList}
                </List>
                </Content>
           
            <KeyboardAvoidingView
            behavior='position' keyboardVerticalOffset={keyboardVerticalOffset}
             >  
             <View style={styles.container}> 
                
             <TextInput
                    placeholder="Name"
                    autoFocus={true} 
                    style={styles.input}
                    value={this.state.text}
                    onChangeText={text => this.onChangeName(text)} 
                    autoCorrect= {false}
                />
                 
                <TextInput
                    placeholder="Add a comment..."
                    autoFocus={true} 
                    style={styles.input}
                    value={this.state.text}
                    onChangeText={text => this.onChangeComment(text)} 
                    autoCorrect= {false}
                />

                
                <TouchableOpacity style={styles.button} onPress={this.submit}>
                    <Text style={styles.text}>Post</Text>
                </TouchableOpacity>

                </View> 

             </KeyboardAvoidingView> 
 

           </Container>            
        );

    }

}

const styles = StyleSheet.create({
    container: {
      backgroundColor: '#FFF',
      flexDirection: 'row',
      borderTopWidth: 1,
      borderColor: '#EEE',
      alignItems: 'center',
      paddingLeft: 15,
    },
    input: {
      flex: 1,
      height: 40,
      fontSize: 15,
    },
    button: {
      height: 40,
      paddingHorizontal: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    inactive: {
      color: '#CCC',
    },
    text: {
      color: '#3F51B5',
      fontWeight: 'bold',
      fontFamily: 'Avenir',
      textAlign: 'center',
      fontSize: 15,
    },
  });




