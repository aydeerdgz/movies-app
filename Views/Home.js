import React, { Component } from 'react';
import { ListItem, View, Header} from 'native-base';
import { SearchBar } from 'react-native-elements';
import {Text, FlatList, ActivityIndicator, TouchableOpacity, StyleSheet} from 'react-native';



export default class Home extends Component {
    constructor() {
        super()
        loading: false,
        this.state = { dataSource: [], searchSource: [] }
    }

    componentDidMount(){
        const url = 'https://www.npoint.io/documents/a21afc7a71ef2b3d4e6a'
        this.setState({loading: true});

        fetch(url)
            .then(response => response.json())
            .then((responseJson) => {
                this.setState({
                    searchSource: responseJson.contents,
                    dataSource: responseJson.contents,
                    loading: false
            });
        })
        .catch((error) => {
            console.log(error)
        })
    }

    filterbyRank = () => {
        const filterData = this.state.dataSource.sort((a, b) => a.rank - b.rank)
        this.setState({
            searchSource: filterData
        })
    }

    filterbyYear = () => {
        const filterData = this.state.dataSource.sort((a, b) => a.year - b.year)
        this.setState({
            searchSource: filterData
        })
    }

    renderItem = ({ item }) => {
        return (
            <View>
                
                <ListItem onPress ={() => {
                    this.props.navigation.navigate('Detail', {Id: item.title})
                }}>
                    <Text>
                        {item.title}
                    </Text>
                </ListItem>
            </View>
            
        )
    }


    renderHeader = () => {
        return (
            <SearchBar        
            placeholder="Type Here..."        
            lightTheme        
            round        
            onChangeText={text => this.searchFilterFunction(text)}
            value={this.state.text}
            autoCorrect={false}             
            />    
        );
    };

    searchFilterFunction = text => {    
        const newData = this.state.dataSource.filter(item => {      
          const itemData = item.title.toUpperCase()
          const textData = text.toUpperCase();
            
           return itemData.indexOf(textData) > -1;    
        });    
        text: text,
        this.setState({ searchSource: newData });  
      };
 

  render() {
    if(this.state.loading){
        return(
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <ActivityIndicator/>
          </View>
        )
    }  

    return (   
        <View>
           <View style ={{flexDirection: "row", alignItems:"center", justifyContent: 'center'}}>
                <TouchableOpacity style={styles.button} onPress={this.filterbyRank}>
                        <Text style={styles.text}>Filter by Rank</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.button}  onPress={this.filterbyYear} >
                        <Text style={styles.text}>Filter by Year</Text>
                </TouchableOpacity>
            </View>

           <FlatList
                    data={this.state.searchSource}
                    renderItem={this.renderItem}
                    keyExtractor={item => item.rank}
                    ListHeaderComponent={this.renderHeader}
            />     
           
        </View>   
    );

    }

  }

  const styles = StyleSheet.create({
    button: {
      height: 40,
      paddingHorizontal: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },

    text: {
        color: '#3F51B5',
        fontWeight: 'bold',
        fontFamily: 'Avenir',
        textAlign: 'center',
        fontSize: 15,
      },
  });
  
