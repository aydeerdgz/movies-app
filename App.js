import React from 'react';
import Home from './Views/Home.js'
import Detail from './Views/Detail.js'
import { createStackNavigator, createAppContainer } from "react-navigation";


// const App = () => <Home />



const AppNavigator = createStackNavigator({
    Home: {
      screen: Home
    }, 
    Detail: {
      screen: Detail
    }
  });

  const AppContainer = createAppContainer(AppNavigator);
  export default AppContainer; 

